# RentalCars

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.5.

## Install dependencies and Development server

Run `npm install` and `npm start` for install dependencies and later run a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## "Backend": Mock Server
The App consumes a mock server of Postman. In the collection there are request examples and the architecture are two:
1.  An Object with all the cars and principal specifications:
```{
    "data":[
        {
            "brand": "bmw",
            "id": 1,
            "img": "https://media.istockphoto.com/photos/rome-italy-white-color-bmw-x1-car-of-second-generation-f48-parked-at-picture-id1137903953?k=20&m=1137903953&s=612x612&w=0&h=69q-imZbpFahWMZVjHaZ3UwIjKP9aIqV44sFzBR9Xy0=",
            "model": "The X1",
            "price": "3540",
            "year": "2021"
        },
        {...},
        {...}
    ]
}
```
2. An Object with secondary specifications about the a car:
```
{
    "id": 1
    "specifications": 
    [
        {
            "title": "Displacement (cc)",
            "data": "1998"
        },
        {
            "title": "Type",
            "data": "8-speed STEPTRONIC Automatic transmission with Sport and Manual shift modes"
        },
        {
            "title": "Horsepower (bhp @ rpm)",
            "data": "228 @ 5000-6000"
        },
        {
            "title": "Compression Ratio (:1)",
            "data": "10.2"
        }
    ],
    "label": "Conquer the city, conquer the world in a versatile, compact BMW.",
}
```

## Angular Architecture
This project has:
1. Modular structure for lazy loading
2. NgRx implementation: Actions, Reducers, Effects and Selectors. There are a implementation Root and a implementation Feature.
3. The design UI/UX use Bootstrap.
4. All values are dynamics: There aren't data "burned".
5. Using custom pipes, components, http request, services, interfaces, etc.

## TODO
1. Finish data typing.
2. Implement eslint and prettier.