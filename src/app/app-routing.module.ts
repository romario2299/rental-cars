import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ErrorComponent } from './components/error/error.component';

const routes: Routes = [
  {
    path: 'dashboard',
    loadChildren: () =>
      import('./modules/dashboard-cars/dashboard-cars.module').then(
        (m) => m.DashboardCarsModule
      ),
  },
  {
    path: ':model/:modelId',
    loadChildren: () =>
      import('./modules/specifications-car/specifications-car.module').then(
        (m) => m.SpecificationsCarModule
      ),
  },
  {
    path: '404',
    pathMatch: 'full',
    component: ErrorComponent,
  },
  {
    path: '**',
    redirectTo: 'dashboard',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
