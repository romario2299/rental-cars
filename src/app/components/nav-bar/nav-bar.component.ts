import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { selectCars } from '../../state/cars.selectors';
import { BaseState } from '../../interfaces/cars.interface';
import { map } from 'rxjs/operators';
import { filterCarsBrand } from '../../state/cars.actions';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css'],
})
export class NavBarComponent implements OnInit {
  brandsList: Observable<any[]>;
  constructor(private store: Store<BaseState>) {
    this.brandsList = this.store.pipe(
      select(selectCars),
      map((carsList) => {
        const brands = carsList.map((car) => car.brand);
        const brandsSet = new Set(brands);
        const result = [...brandsSet];
        return result;
      })
    );
  }

  ngOnInit(): void {}

  filterBrand(brand: string) {
    this.store.dispatch(filterCarsBrand({ filter: brand }));
  }
}
