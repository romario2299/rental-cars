import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarouselCarComponent } from './carousel-car.component';

describe('CarouselCarComponent', () => {
  let component: CarouselCarComponent;
  let fixture: ComponentFixture<CarouselCarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarouselCarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarouselCarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
