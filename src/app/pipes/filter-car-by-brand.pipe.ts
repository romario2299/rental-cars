import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterCarByBrand',
})
export class FilterCarByBrandPipe implements PipeTransform {
  transform(carsList, valueToSearch): unknown {
    if (valueToSearch?.length > 1) {
      const usersFilter = carsList.filter((car) => {
        const subString = car.brand.substring(0, valueToSearch.length);
        if (subString === valueToSearch.toLowerCase()) return car;
      });
      return usersFilter.filter((value) => value);
    } else {
      return carsList;
    }
  }
}
