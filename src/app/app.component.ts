import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { BaseState } from './interfaces/cars.interface';
import { getCars } from './state/cars.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  constructor(private store: Store<BaseState>) {
    this.store.dispatch(getCars());
  }
}
