export interface BaseState {
  get: {
    isLoading: boolean;
    error: Error;
  };
  data: [];
}

export interface initialStateI extends BaseState {
  dataFilterBrand: [];
}

export interface Cars {}

export interface Error {
  messageError: string;
  statusCode: number;
}

export interface NavRouteCar {
  model: string;
  modelId: string;
}
