import { TestBed } from '@angular/core/testing';

import { RequestCarService } from './request-car.service';

describe('RequestCarService', () => {
  let service: RequestCarService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RequestCarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
