import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class RequestCarService {
  apiUrl: string = environment.webService;

  constructor(private httpClient: HttpClient) {}

  public gerOneCar(model: string, modelId: string): Observable<any> {
    const path = `/cars/${modelId}`;
    return this.httpClient.get(`${this.apiUrl}${path}`);
  }
}
