import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecificationsCarComponent } from './specifications-car.component';

describe('SpecificationsCarComponent', () => {
  let component: SpecificationsCarComponent;
  let fixture: ComponentFixture<SpecificationsCarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpecificationsCarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecificationsCarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
