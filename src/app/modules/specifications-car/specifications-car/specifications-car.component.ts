import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { BaseState, NavRouteCar } from '../../../interfaces/cars.interface';
import { getOneCar } from '../state/car.actions';
import { selectOneCar, selectLoadingOneCar } from '../state/car.selectors';

@Component({
  selector: 'app-specifications-car',
  templateUrl: './specifications-car.component.html',
  styleUrls: ['./specifications-car.component.css'],
})
export class SpecificationsCarComponent implements OnInit {
  paramsSubscription: Subscription;
  model: NavRouteCar;
  oneCarSubscription: Subscription;
  oneCar: any;
  isLoading = this.store.pipe(select(selectLoadingOneCar));
  constructor(
    private store: Store<BaseState>,
    private activatedRoute: ActivatedRoute
  ) {
    this.paramsSubscription = this.activatedRoute.paramMap.subscribe(
      ({ params }: any) => {
        this.model = params;
      }
    );
    this.store.dispatch(getOneCar({ req: this.model }));
  }
  ngOnInit(): void {
    this.oneCarSubscription = this.store
      .pipe(
        select(selectOneCar),
        filter((data) => data.id)
      )
      .subscribe((data) => (this.oneCar = data));
  }

  ngOnDestroy(): void {
    this.paramsSubscription.unsubscribe();
    this.oneCarSubscription.unsubscribe();
  }
}
