import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { switchMap, map, catchError, tap } from 'rxjs/operators';
import { Actions, createEffect, ofType } from '@ngrx/effects';

import { getOneCar, getOneCarSuccess, getOneCarError } from './car.actions';
import { RequestCarService } from '../services/request-car.service';
import { Error } from '../../../interfaces/cars.interface';

@Injectable()
export class CarEffects {
  constructor(
    private actions$: Actions,
    private router: Router,
    private requestCarService: RequestCarService
  ) {}

  GetCarsAction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getOneCar),
      switchMap(({ req }) =>
        this.requestCarService.gerOneCar(req.model, req.modelId).pipe(
          map((data) => getOneCarSuccess({ car: data })),
          catchError((res) => {
            const error: Error = {
              messageError: res.message,
              statusCode: res.status,
            };
            return of(getOneCarError({ error }));
          })
        )
      )
    )
  );

  GetCarsErrorAction$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(getOneCarError),
        tap(() => {
          this.router.navigate(['404']);
        })
      ),
    { dispatch: false }
  );
}
