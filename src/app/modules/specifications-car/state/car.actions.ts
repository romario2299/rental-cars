import { createAction, props } from '@ngrx/store';
import { Cars, Error, NavRouteCar } from '../../../interfaces/cars.interface';

export const appKey = 'oneCar';

export const getOneCar = createAction(
  `[${appKey}] Get Info`,
  props<{ req: NavRouteCar }>()
);
export const getOneCarSuccess = createAction(
  `[${appKey}] Get Info Success`,
  props<{ car: Cars }>()
);
export const getOneCarError = createAction(
  `[${appKey}] Get Info Error`,
  props<{ error: Error }>()
);
