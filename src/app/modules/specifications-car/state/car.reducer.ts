import { createReducer, on } from '@ngrx/store';
import { getOneCar, getOneCarSuccess, getOneCarError } from './car.actions';
import { BaseState } from '../../../interfaces/cars.interface';

export const initialState: BaseState = {
  data: [],
  get: {
    isLoading: false,
    error: {
      messageError: '',
      statusCode: 0,
    },
  },
};

const _oneCarReducer = createReducer(
  initialState,
  on(getOneCar, (state) => {
    return {
      ...state,
      get: {
        ...state.get,
        isLoading: true,
      },
    };
  }),
  on(getOneCarSuccess, (state, { car }) => {
    return {
      ...state,
      data: car,
      get: {
        ...state.get,
        isLoading: false,
      },
    };
  }),
  on(getOneCarError, (state, { error }) => {
    return {
      ...state,
      get: {
        error,
        isLoading: false,
      },
    };
  })
);

export function oneCarReducer(state, action) {
  return _oneCarReducer(state, action);
}
