import { createFeatureSelector, createSelector } from '@ngrx/store';
import { BaseState } from '../../../interfaces/cars.interface';

const featureKey = 'oneCar';

export const selectFeatureCar = createFeatureSelector<BaseState>(featureKey);
export const selectFeatureCars =
  createFeatureSelector<BaseState>('carsReducer');

export const selectOneCar = createSelector(
  selectFeatureCar,
  selectFeatureCars,
  (state: BaseState, state2: BaseState) => {
    const carsList = state2.data;
    const oneCar: any = state.data;
    if (carsList.length && oneCar.id) {
      let result = {};
      result = carsList.find((car: any) => car.id === oneCar.id);
      return { ...result, ...oneCar };
    } else {
      return {};
    }
  }
);

export const selectLoadingOneCar = createSelector(
  selectFeatureCar,
  (state: BaseState) => state.get.isLoading
);
