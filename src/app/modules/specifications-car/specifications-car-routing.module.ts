import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SpecificationsCarComponent } from './specifications-car/specifications-car.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: SpecificationsCarComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SpecificationsCarRoutingModule {}
