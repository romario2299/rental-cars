import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { SpecificationsCarRoutingModule } from './specifications-car-routing.module';
import { SpecificationsCarComponent } from './specifications-car/specifications-car.component';
import { appKey } from './state/car.actions';
import { oneCarReducer } from './state/car.reducer';
import { RequestCarService } from './services/request-car.service';
import { CarEffects } from './state/car.effects';

@NgModule({
  declarations: [SpecificationsCarComponent],
  imports: [
    CommonModule,
    SpecificationsCarRoutingModule,
    StoreModule.forFeature(appKey, oneCarReducer),
    EffectsModule.forFeature([CarEffects]),
  ],
  providers: [RequestCarService],
})
export class SpecificationsCarModule {}
