import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { BaseState } from '../../../interfaces/cars.interface';
import {
  selectCarsFilter,
  selectLoading,
  selectdataFilterBrand,
} from '../../../state/cars.selectors';
import { filterCarsBrand } from '../../../state/cars.actions';

@Component({
  selector: 'app-dashboard-cars',
  templateUrl: './dashboard-cars.component.html',
  styleUrls: ['./dashboard-cars.component.css'],
})
export class DashboardCarsComponent implements OnInit, AfterViewInit {
  @ViewChild('searchBrand') inputSearchBrand;
  carsList: Observable<any[]>;
  valueToSearch = '';
  isLoading = this.store.pipe(select(selectLoading));
  isDataFilterBrand = this.store.pipe(select(selectdataFilterBrand));
  constructor(private store: Store<BaseState>, private router: Router) {
    this.carsList = this.store.pipe(
      select(selectCarsFilter),
      filter((data) => data.length != 0)
    );
  }

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    if (this.inputSearchBrand) this.inputSearchBrand.nativeElement.value = '';
  }

  oneCarSelected(brand: string, idCar: number) {
    this.router.navigate([brand, idCar]);
  }

  deleteFilterBrand() {
    this.store.dispatch(filterCarsBrand({ filter: '' }));
  }
}
