import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardCarsComponent } from './dashboard-cars/dashboard-cars.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: DashboardCarsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardCarsRoutingModule {}
