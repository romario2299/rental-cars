import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardCarsRoutingModule } from './dashboard-cars-routing.module';
import { DashboardCarsComponent } from './dashboard-cars/dashboard-cars.component';
import { FormsModule } from '@angular/forms';
import { FilterCarByBrandPipe } from '../../pipes/filter-car-by-brand.pipe';

@NgModule({
  declarations: [DashboardCarsComponent, FilterCarByBrandPipe],
  imports: [CommonModule, DashboardCarsRoutingModule, FormsModule],
})
export class DashboardCarsModule {}
