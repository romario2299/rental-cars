import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class RequestService {
  apiUrl: string = environment.webService;

  constructor(private httpClient: HttpClient) {}

  public gerCars(): Observable<any> {
    const path = '/cars';
    return this.httpClient.get(`${this.apiUrl}${path}`);
  }
}
