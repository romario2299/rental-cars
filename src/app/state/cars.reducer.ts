import { createReducer, on } from '@ngrx/store';
import {
  getCars,
  getCarsSuccess,
  getCarsError,
  filterCarsBrand,
} from './cars.actions';
import { initialStateI } from '../interfaces/cars.interface';

export const initialState: initialStateI = {
  data: [],
  get: {
    isLoading: false,
    error: {
      messageError: null,
      statusCode: null,
    },
  },
  dataFilterBrand: [],
};

const _carsReducer = createReducer(
  initialState,
  on(getCars, (state) => {
    return {
      ...state,
      get: {
        ...state.get,
        isLoading: true,
      },
    };
  }),
  on(getCarsSuccess, (state, { cars }) => {
    return {
      ...state,
      data: cars,
      get: {
        ...state.get,
        isLoading: false,
      },
    };
  }),
  on(getCarsError, (state, { error }) => {
    return {
      ...state,
      get: {
        error,
        isLoading: false,
      },
    };
  }),
  on(filterCarsBrand, (state: any, { filter }) => {
    const usersFilter = state.data.filter((car) => {
      if (car.brand === filter) return car;
    });
    return {
      ...state,
      dataFilterBrand: [...usersFilter],
    };
  })
);

export function carsReducer(state, action) {
  return _carsReducer(state, action);
}
