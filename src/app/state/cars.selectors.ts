import { createFeatureSelector, createSelector } from '@ngrx/store';
import { initialStateI } from '../interfaces/cars.interface';

const featureKey = 'carsReducer';

export const selectFeature = createFeatureSelector<initialStateI>(featureKey);

export const selectCarsFilter = createSelector(
  selectFeature,
  (state: initialStateI) => {
    if (state.dataFilterBrand.length) {
      return state.dataFilterBrand;
    } else if (state.data.length) {
      return state.data;
    } else {
      return [];
    }
  }
);

export const selectCars = createSelector(
  selectFeature,
  (state: initialStateI) => (state ? state.data : [])
);

export const selectdataFilterBrand = createSelector(
  selectFeature,
  (state: initialStateI) => (state ? state.dataFilterBrand.length : 0)
);

export const selectLoading = createSelector(
  selectFeature,
  (state: initialStateI) => state.get.isLoading
);
