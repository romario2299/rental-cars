import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { switchMap, map, catchError, tap } from 'rxjs/operators';
import { Actions, createEffect, ofType } from '@ngrx/effects';

import { getCars, getCarsSuccess, getCarsError } from './cars.actions';
import { RequestService } from '../services/request.service';
import { Error } from '../interfaces/cars.interface';

@Injectable()
export class CarsEffects {
  constructor(
    private actions$: Actions,
    private router: Router,
    private requestService: RequestService
  ) {}

  GetCarsAction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getCars),
      switchMap(() =>
        this.requestService.gerCars().pipe(
          map(({ data }) => getCarsSuccess({ cars: data })),
          catchError((res) => {
            const error: Error = {
              messageError: res.message,
              statusCode: res.status,
            };
            return of(getCarsError({ error }));
          })
        )
      )
    )
  );

  GetCarsErrorAction$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(getCarsError),
        tap(() => {
          this.router.navigate(['404']);
        })
      ),
    { dispatch: false }
  );
}
