import { createAction, props } from '@ngrx/store';
import { Cars, Error } from '../interfaces/cars.interface';

const appKey = 'Cars';

export const getCars = createAction(`[${appKey}] Get Info`);
export const getCarsSuccess = createAction(
  `[${appKey}] Get Info Success`,
  props<{ cars: Cars }>()
);
export const getCarsError = createAction(
  `[${appKey}] Get Info Error`,
  props<{ error: Error }>()
);
export const filterCarsBrand = createAction(
  `[${appKey}] Filter Cars`,
  props<{ filter: string }>()
);
